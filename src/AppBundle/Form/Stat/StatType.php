<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 16.06.2016
 * Time: 11:02
 */


namespace AppBundle\Form\Stat;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class StatType extends AbstractType
{

	public static $currencyList = [
		'RUB' => 'Russian ruble',
		'EUR' => 'Euro',
		'USD' => 'American dollar',
	];

	public function buildForm(FormBuilderInterface $builder, array $options)
	{

		$dateTimeFieldOptions = [
			'widget' => 'single_text',
			'format' => 'dd.MM.yyyy',
			'attr'   => ['class' => 'datepicker'],
		];

		$builder
			->add('currency', ChoiceType::class, [
				'label'    => 'Currency',
				'choices'  => self::$currencyList,
				'multiple' => true,
			])
			->add('date_start', DateTimeType::class, $dateTimeFieldOptions)
			->add('date_end', DateTimeType::class, $dateTimeFieldOptions)
			->add('show_chart', SubmitType::class)
			->add('export_xml', SubmitType::class);
	}

}
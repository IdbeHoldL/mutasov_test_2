<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 18.06.2016
 * Time: 18:22
 */

namespace AppBundle\Api;

use Circle\RestClientBundle\Services\RestClient;
use Symfony\Component\PropertyAccess\PropertyAccess;
use DateTime;
use DateInterval;

class BankUaApi
{
	/**
	 * @var RestClient
	 */
	public $restClient;

	/**
	 * @var String
	 */
	public $apiUrl;

	public function __construct(RestClient $restClient, $apiUrl)
	{
		$this->restClient = $restClient;
		$this->apiUrl     = $apiUrl;
	}

	/**
	 * make get Request to api
	 * @param $method
	 * @param $params
	 * @return mixed
	 */
	private function get($method, $params)
	{
		$url      = $this->apiUrl . $method . '?json' . '&' . http_build_query($params);
		$response = $this->restClient->get($url);

		return json_decode($response->getContent(), true);
	}

	/**
	 * Get exchange rates
	 * @param DateTime $dateStart
	 * @param DateTime $dateEnd
	 * @param array    $currencyCodes
	 * @return array
	 */
	public function getCurrencyExchangeRatesByPeriod(DateTime $dateStart, DateTime $dateEnd, $currencyCodes)
	{
		$result       = [];
		$dateInterval = DateInterval::createFromDateString('1 day');
		$period       = new \DatePeriod($dateStart, $dateInterval, $dateEnd->add($dateInterval));

		/* @var $dateTime DateTime */
		foreach ($currencyCodes as $currencyCode) {
			foreach ($period as $dateTime) {
				$result[$currencyCode][$dateTime->format('Y-m-d H:i:s')] = $this->getCurrencyExchangeRateByDate($dateTime, $currencyCode);
			}
		}

		return $result;
	}

	/**
	 * @param DateTime $date
	 * @param string   $currencyCode
	 * @return mixed
	 */
	public function getCurrencyExchangeRateByDate(DateTime $date, $currencyCode)
	{
		$result = $this->get('statdirectory/exchange', [
			'date'    => $date->format('Ymd'),
			'valcode' => $currencyCode
		]);

		$propAccess = PropertyAccess::createPropertyAccessor();

		return $propAccess->getValue($result, '[0][rate]');
	}
}
<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use AppBundle\Form\Stat\StatType;

class StatController extends Controller
{
	/**
	 * Exchange rates Form with Chart
	 * @Route("/", name="home")
	 *
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function indexAction(Request $request)
	{

		$form = $this->createForm(StatType::class);
		$form->handleRequest($request);

		if ($form->isValid()) {
			/* @var $bankApi \AppBundle\Api\BankUaApi */
			$bankApi       = $this->container->get('bank_api');
			$formData      = $form->getData();
			$exchangeRates = $bankApi->getCurrencyExchangeRatesByPeriod($formData['date_start'], $formData['date_end'], $formData['currency']);

			if ($form->get('export_xml')->isClicked()) {
				return $this->xmlExport($exchangeRates);
			}
		}

		return $this->render('stat/index.html.twig', [
			'form'          => $form->createView(),
			'exchangeRates' => isset($exchangeRates) ? $exchangeRates : false
		]);
	}

	/**
	 * Export exchange rates data to xml file
	 * @param $exchangeRates
	 * @return Response
	 */
	private function xmlExport($exchangeRates)
	{
		$xml = new \SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' standalone='yes'?><items></items>");
		foreach ($exchangeRates as $currencyCode => $rates) {
			$currencyNode = $xml->addChild($currencyCode);
			foreach ($rates as $dateTime => $rate) {
				$item = $currencyNode->addChild('item');
				$item->addChild('date', $dateTime);
				$item->addChild('rate', $rate);
			}
		}

		$response           = new Response($xml->asXML());
		$contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'exchange_rates.xml');
		$response->headers->set('Content-Disposition', $contentDisposition);

		return $response;
	}
}

/**
 * Created by q on 19.06.2016.
 */

$(function () {
	// chart render
	var chartData = [];

	for (var currencyCode in exchangeRates) {
		var lineData = {
			type        : 'line',
			xValueType  : "dateTime",
			showInLegend: true,
			legendText  : currencyCode,
			dataPoints  : []
		};

		for (var dateTime in exchangeRates[currencyCode]) {
			lineData.dataPoints.push({
				x: new Date(dateTime.split(' ')[0]),
				y: exchangeRates[currencyCode][dateTime]
			});
		}
		chartData.push(lineData);
	}

	var chart = new CanvasJS.Chart("chart_container",
		{
			zoomEnabled: true,
			title      : {
				text: "Exchange Rates"
			},
			axisX      : {
				intervalType     : "day",
				valueFormatString: "DD.MM.YYYY"
			},
			axisY      : {
				includeZero: false
			},
			data       : chartData
		});
	chart.render();

});